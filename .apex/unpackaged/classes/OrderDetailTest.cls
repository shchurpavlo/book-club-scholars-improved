@isTest
public class OrderDetailTest {

    @testSetup static void setup() {

        insert ( new Product2__c(Name = 'testProduct2setup') );
        Product2__c product2 = [SELECT Units_Sold__c, Units_On_Order__c FROM Product2__c WHERE Name = 'testProduct2setup'];

        Contact cont_1 = new Contact(LastName = 'cont_1', FirstName = 'fromSetup');
        Contact cont_2 = new Contact(LastName = 'cont_2', FirstName = 'fromSetup');
        Contact cont_3 = new Contact(LastName = 'cont_1', FirstName = 'fromSetup');
        Contact cont_4 = new Contact(LastName = 'cont_4', FirstName = 'fromSetup');
        Contact cont_5 = new Contact(LastName = 'cont_5', FirstName = 'fromSetup');
        Contact cont_6 = new Contact(LastName = 'cont_1', FirstName = 'fromSetup');

        insert new List<Contact>{cont_1, cont_2, cont_3, cont_4, cont_5, cont_6};

        Rank__c setting_1 = new Rank__c(Name = 'Gold', Reward__c = '5');
        Rank__c setting_2 = new Rank__c(Name = 'Silver', Reward__c = '3');
        Rank__c setting_3 = new Rank__c(Name = 'Bronze', Reward__c = '1');

        insert new List<Rank__c>{setting_1, setting_2, setting_3};

        Account acct = new Account(Name = 'testAccountName');
        insert ( acct );

        Contact cont = new Contact(LastName = 'testContactName');
        insert ( cont );

        Book_Order__c testBookOrder = new Book_Order__c(Name = 'testBookOrderName', Account__c = acct.Id);
        insert ( testBookOrder );

        Order_Line_Item__c testOrderItem_1 = new Order_Line_Item__c(Name = 'testName_1', Shipping_Status__c = 'Shipped', Student__c =  cont.Id,
                                                                    Level__c = 'Gold', Quantity__c = 20, Book_Order__c = testBookOrder.Id, Product__c = product2.Id);

        Order_Line_Item__c testOrderItem_2 = new Order_Line_Item__c(Name = 'testName_2', Shipping_Status__c = 'Pending', Student__c =  cont.Id,
                                                                    Level__c = 'Gold', Quantity__c = 20, Book_Order__c = testBookOrder.Id, Product__c = product2.Id);

        Order_Line_Item__c testOrderItem_3 = new Order_Line_Item__c(Name = 'testName_3', Shipping_Status__c = 'Shipped', Student__c =  cont.Id,
                                                                    Level__c = 'Gold', Quantity__c = 20, Book_Order__c = testBookOrder.Id, Product__c = product2.Id);

        Order_Line_Item__c testOrderItem_4 = new Order_Line_Item__c(Name = 'testName_4', Shipping_Status__c = 'Shipment', Student__c =  cont.Id,
                                                                    Level__c = 'Gold', Quantity__c = 20, Book_Order__c = testBookOrder.Id, Product__c = product2.Id);

		Order_Line_Item__c testOrderItem_5 = new Order_Line_Item__c(Name = 'testName_5', Shipping_Status__c = 'Shipped', Student__c =  cont.Id,
                													Level__c = 'Gold', Quantity__c = 20, Book_Order__c = testBookOrder.Id, Product__c = product2.Id);

        Order_Line_Item__c testOrderItem_6 = new Order_Line_Item__c(Name = 'testName_6', Shipping_Status__c = 'Pending', Student__c =  cont.Id,
                                                                    Level__c = 'Gold', Quantity__c = 20, Book_Order__c = testBookOrder.Id, Product__c = product2.Id);

        insert new List<Order_Line_Item__c>{testOrderItem_1,testOrderItem_2,testOrderItem_3,testOrderItem_4,testOrderItem_5,testOrderItem_6};
    }

    @isTest static void insert_orderLineItem_RewardsPointsAreCalculated_Shipped(){

        Account acct = [SELECT Name FROM Account WHERE Name = 'testAccountName'];

        Book_Order__c bookOrderFromSetup = [SELECT Name FROM Book_Order__c WHERE Name = 'testBookOrderName' AND Account__c = :acct.Id];

        Order_Line_Item__c testOrderItem = new Order_Line_Item__c(Name = 'test', Shipping_Status__c = 'Shipped',
                                                                  Level__c = 'Gold', Quantity__c = 10, Book_Order__c = bookOrderFromSetup.Id);

        Test.startTest();
        insert testOrderItem;
        Test.stopTest();

        System.assertEquals(50, [SELECT Rewards_Points__c FROM Order_Line_Item__c WHERE Name = 'test'].Rewards_Points__c);
    }

    @isTest static void insert_orderLineItems_RewardsPointsAreCalculated_Cancelled(){

        Account acct = [SELECT Name FROM Account WHERE Name = 'testAccountName'];

        Book_Order__c bookOrderFromSetup = [SELECT Name FROM Book_Order__c WHERE Name = 'testBookOrderName' AND Account__c = :acct.Id];

        Order_Line_Item__c testOrderItem = new Order_Line_Item__c(Name = 'test', Shipping_Status__c = 'Cancelled',
                Level__c = 'Gold', Quantity__c = 10, Book_Order__c = bookOrderFromSetup.Id);

        Test.startTest();
        insert testOrderItem;
        Test.stopTest();

        System.assertEquals(null, [SELECT Rewards_Points__c FROM Order_Line_Item__c WHERE Name = 'test'].Rewards_Points__c);
    }

    @isTest static void insert_orderLineItems_RewardsPointsAreCalculated_null(){

        Account acct = [SELECT Name FROM Account WHERE Name = 'testAccountName'];

        Book_Order__c bookOrderFromSetup = [SELECT Name FROM Book_Order__c WHERE Name = 'testBookOrderName' AND Account__c = :acct.Id];

        Order_Line_Item__c testOrderItem = new Order_Line_Item__c(Name = 'test', Level__c = 'Gold', Quantity__c = 10, Book_Order__c = bookOrderFromSetup.Id);

        Test.startTest();
        insert testOrderItem;
        Test.stopTest();

        System.assertEquals(null, [SELECT Rewards_Points__c FROM Order_Line_Item__c WHERE Name = 'test'].Rewards_Points__c);

    }

    @isTest static void insert_orderLineItem_RewardsPointsAreNotCalculated_Status_Shipment(){

        Account acct = [SELECT Name FROM Account WHERE Name = 'testAccountName'];

        Book_Order__c bookOrderFromSetup = [SELECT Name FROM Book_Order__c WHERE Name = 'testBookOrderName' AND Account__c = :acct.Id];

        Order_Line_Item__c testOrderItem = new Order_Line_Item__c(Name = 'test', Shipping_Status__c = 'Shipment',
                                                                  Level__c = 'Gold', Quantity__c = 10, Book_Order__c = bookOrderFromSetup.Id);

        Test.startTest();
        insert ( testOrderItem );
        Test.stopTest();

        System.assertEquals(null, [SELECT Rewards_Points__c FROM Order_Line_Item__c WHERE Name = 'test'].Rewards_Points__c);

    }

    @isTest static void update_orderLineItem_RewardsPointsAreCalculated_Status_Shipped(){

        Order_Line_Item__c order_line_item = [SELECT Shipping_Status__c, Rewards_Points__c FROM Order_Line_Item__c WHERE Name = 'testName_4'];
        System.assertEquals(null, order_line_item.Rewards_Points__c);

        order_line_item.Shipping_Status__c = 'Shipped';

        Test.startTest();
        update ( order_line_item );
        Test.stopTest();

        System.assertEquals(100, [SELECT Rewards_Points__c FROM Order_Line_Item__c WHERE Name = 'testName_3'].Rewards_Points__c);

    }

    @isTest static void update_orderLineItem_RewardsPointsAreNotCalculated_Status_Shipment(){

        Order_Line_Item__c order_line_item = [SELECT Shipping_Status__c, Rewards_Points__c FROM Order_Line_Item__c WHERE Name = 'testName_1'];
        System.assertEquals(100, order_line_item.Rewards_Points__c);

        order_line_item.Shipping_Status__c = 'Shipment';

        Test.startTest();
        update ( order_line_item );
        Test.stopTest();

        System.assertEquals(null, [SELECT Rewards_Points__c FROM Order_Line_Item__c WHERE Name = 'testName_1'].Rewards_Points__c);
    }

    @isTest static void update_orderLineItem_RewardsPointsAreNotCalculated_Status_null(){

        Order_Line_Item__c order_line_item = [SELECT Shipping_Status__c, Rewards_Points__c FROM Order_Line_Item__c WHERE Name = 'testName_1'];
        System.assertEquals(100, order_line_item.Rewards_Points__c);

        order_line_item.Shipping_Status__c = null;

        Test.startTest();
        update ( order_line_item );
        Test.stopTest();

        System.assertEquals(null, [SELECT Rewards_Points__c FROM Order_Line_Item__c WHERE Name = 'testName_1'].Rewards_Points__c);
    }

    @isTest static void update_orderLineItem_RewardsPointsAreCalculated_Status_From_Shipment_To_Shipped(){

        Order_Line_Item__c orderLineItemFromSetup = [SELECT Shipping_Status__c FROM Order_Line_Item__c WHERE Name = 'testName_3'];

        orderLineItemFromSetup.Shipping_Status__c = 'Shipped';

        Test.startTest();
        update ( orderLineItemFromSetup );
        Test.stopTest();

        System.assertEquals(100, [SELECT Rewards_Points__c FROM Order_Line_Item__c WHERE Name = 'testName_1'].Rewards_Points__c);
    }

    @isTest static void insert_orderLineItems_ContactRewardsPointsAreCalculated(){

        insert (new Contact(LastName = 'testContact12'));
        Contact contact = [SELECT Rewards_Points__c FROM Contact WHERE LastName = 'testContact12'];

        Account acct = [SELECT Name FROM Account WHERE Name = 'testAccountName'];

        Book_Order__c bookOrderFromSetup = [SELECT Name FROM Book_Order__c WHERE Name = 'testBookOrderName' AND Account__c = :acct.Id];

        Order_Line_Item__c testOrderItem_1 = new Order_Line_Item__c(Name = 'testOrderItem', Shipping_Status__c = 'Shipped', Student__c = contact.Id,
                                                                    Level__c = 'Gold', Quantity__c = 10, Book_Order__c = bookOrderFromSetup.Id);

        Order_Line_Item__c testOrderItem_2 = new Order_Line_Item__c(Name = 'testOrderItem', Shipping_Status__c = 'Shipped', Student__c =  contact.Id,
                                                                    Level__c = 'Gold', Quantity__c = 10, Book_Order__c = bookOrderFromSetup.Id);

        Order_Line_Item__c testOrderItem_3 = new Order_Line_Item__c(Name = 'testOrderItem', Shipping_Status__c = 'Shipped', Student__c =  contact.Id,
                                                                    Level__c = 'Gold', Quantity__c = 10, Book_Order__c = bookOrderFromSetup.Id);

        Test.startTest();
        insert new List<Order_Line_Item__c>{testOrderItem_1, testOrderItem_2, testOrderItem_3};
        Test.stopTest();

        System.assertEquals(150, [SELECT Rewards_Points__c FROM Contact WHERE LastName = 'testContact12'].Rewards_Points__c);
    }

    @isTest static void delete_orderLineItem_ContactRewardsPointsAreRecalculated(){

        List<Order_Line_Item__c> orderLineItemsToUpdate = new List<Order_Line_Item__c>();
        Set<Id> contactIds = new Set<Id>();

        orderLineItemsToUpdate = [SELECT Quantity__c, Rewards_Points__c, Student__c FROM Order_Line_Item__c WHERE Quantity__c = 20];

        for(Order_Line_Item__c orderItem :orderLineItemsToUpdate){contactIds.add(orderItem.Student__c);}

		Test.startTest();
        Database.delete(orderLineItemsToUpdate.get(1));
        Test.stopTest();

        System.assertEquals(300, [SELECT Rewards_Points__c FROM Contact WHERE Id IN :contactIds].Rewards_Points__c);
    }

    @isTest static void deleteAllRelated_orderLineItems_ContactRewardsPointsAreRecalculated(){

        List<Order_Line_Item__c> orderLineItemsToUpdate = new List<Order_Line_Item__c>();
        Set<Id> contactIds = new Set<Id>();

        orderLineItemsToUpdate = [SELECT Quantity__c, Rewards_Points__c, Student__c FROM Order_Line_Item__c WHERE Quantity__c = 20];

        for(Order_Line_Item__c orderItem :orderLineItemsToUpdate){contactIds.add(orderItem.Student__c);}

        Test.startTest();
        Database.delete(orderLineItemsToUpdate);
        Test.stopTest();

        System.assertEquals(null, [SELECT Rewards_Points__c FROM Contact WHERE Id IN :contactIds].Rewards_Points__c);
    }

    @isTest static void changeContactLookup_orderLineItems_ContactRewardsPointsAreRecalculated(){

        insert (new Contact(LastName = 'testContact203'));
        Contact contact = [SELECT Rewards_Points__c FROM Contact WHERE LastName = 'testContact203'];

        List<Order_Line_Item__c> orderLineItemsToUpdate = new List<Order_Line_Item__c>();

        orderLineItemsToUpdate = [SELECT Quantity__c, Rewards_Points__c, Student__c FROM Order_Line_Item__c WHERE Quantity__c = 20];

        System.assertEquals(null, contact.Rewards_Points__c);
        System.assertEquals(300, [SELECT Rewards_Points__c FROM Contact WHERE Name = 'testContactName'].Rewards_Points__c);

        orderLineItemsToUpdate.get(2).Student__c = contact.Id;

        Test.startTest();
        update ( orderLineItemsToUpdate );
        Test.stopTest();

        System.assertEquals(100, [SELECT Rewards_Points__c FROM Contact WHERE LastName = 'testContact203'].Rewards_Points__c);
        System.assertEquals(200, [SELECT Rewards_Points__c FROM Contact WHERE Name = 'testContactName'].Rewards_Points__c);
    }

    @isTest static void update_orderLineItem_ContactRewardsPointsAreRecalculated_ShippingStatus_Shipment(){

        List<Order_Line_Item__c> orderLineItemsToUpdate = new List<Order_Line_Item__c>();

        orderLineItemsToUpdate = [SELECT Quantity__c, Rewards_Points__c, Student__c FROM Order_Line_Item__c WHERE Quantity__c = 20];

        System.assertEquals(300, [SELECT Rewards_Points__c FROM Contact WHERE Name = 'testContactName'].Rewards_Points__c);

        orderLineItemsToUpdate.get(2).Shipping_Status__c = 'Shipment';

        Test.startTest();
        update ( orderLineItemsToUpdate );
        Test.stopTest();

        System.assertEquals(200, [SELECT Rewards_Points__c FROM Contact WHERE  Name = 'testContactName'].Rewards_Points__c);
    }

    @isTest static void update_orderLineItem_ContactRewardsPointsAreRecalculated_ShippingStatus_Cancelled(){

        List<Order_Line_Item__c> orderLineItemsToUpdate = new List<Order_Line_Item__c>();

        orderLineItemsToUpdate = [SELECT Quantity__c, Rewards_Points__c, Student__c FROM Order_Line_Item__c WHERE Quantity__c = 20];

        System.assertEquals(300, [SELECT Rewards_Points__c FROM Contact WHERE Name = 'testContactName'].Rewards_Points__c);

        orderLineItemsToUpdate.get(1).Shipping_Status__c = 'Cancelled';

        Test.startTest();
        update ( orderLineItemsToUpdate );
        Test.stopTest();

        System.assertEquals(300, [SELECT Rewards_Points__c FROM Contact WHERE Name = 'testContactName'].Rewards_Points__c);
    }

    @isTest static void update_orderLineItem_ContactRewardsPointsAreRecalculated_ShippingStatus_null(){

        List<Order_Line_Item__c> orderLineItemsToUpdate = new List<Order_Line_Item__c>();

        orderLineItemsToUpdate = [SELECT Quantity__c, Rewards_Points__c, Student__c FROM Order_Line_Item__c WHERE Quantity__c = 20];

        System.assertEquals(300, [SELECT Rewards_Points__c FROM Contact WHERE Name = 'testContactName'].Rewards_Points__c);

        orderLineItemsToUpdate.get(1).Shipping_Status__c = null;

        Test.startTest();
        update ( orderLineItemsToUpdate );
        Test.stopTest();

        System.assertEquals(300, [SELECT Rewards_Points__c FROM Contact WHERE Name = 'testContactName'].Rewards_Points__c);
    }

    @isTest static void update_orderLineItem_ContactRewardsPointsAreRecalculated_ShippingStatus_Shipped(){

        List<Order_Line_Item__c> orderLineItemsToUpdate = new List<Order_Line_Item__c>();

        orderLineItemsToUpdate = [SELECT Quantity__c, Rewards_Points__c, Student__c FROM Order_Line_Item__c WHERE Quantity__c = 20];

        System.assertEquals(300, [SELECT Rewards_Points__c FROM Contact WHERE Name = 'testContactName'].Rewards_Points__c);

        orderLineItemsToUpdate.get(2).Shipping_Status__c = 'Shipped';

        Test.startTest();
        update ( orderLineItemsToUpdate );
        Test.stopTest();

        System.assertEquals(300, [SELECT Rewards_Points__c FROM Contact WHERE Name = 'testContactName'].Rewards_Points__c);
    }

    @isTest static void update_orderLineItem_threeOrderLineItemsForContact_recalculateContactRewardsPoints(){

        List<Order_Line_Item__c> orderLineItemsToUpdate = new List<Order_Line_Item__c>();
        Set<Id> contactIds = new Set<Id>();

        orderLineItemsToUpdate = [SELECT Quantity__c, Rewards_Points__c, Student__c FROM Order_Line_Item__c WHERE Quantity__c = 20];

        for(Order_Line_Item__c orderItem :orderLineItemsToUpdate){contactIds.add(orderItem.Student__c);}

        System.assertEquals(300, [SELECT Rewards_Points__c FROM Contact WHERE Id IN :contactIds].Rewards_Points__c);

        orderLineItemsToUpdate.get(2).Quantity__c = 100;

		Test.startTest();
        update ( orderLineItemsToUpdate );
        Test.stopTest();

        System.assertEquals(700, [SELECT Rewards_Points__c FROM Contact WHERE Id IN :contactIds].Rewards_Points__c);
    }

    @isTest static void update_orderLineItem_checkContactParameters_Cancelled(){

        List<Order_Line_Item__c> orderLineItemsToUpdate = new List<Order_Line_Item__c>();
        Set<Id> contactIds = new Set<Id>();

        orderLineItemsToUpdate = [SELECT Quantity__c, Rewards_Points__c, Student__c FROM Order_Line_Item__c WHERE Quantity__c = 20];

        for(Order_Line_Item__c orderItem :orderLineItemsToUpdate){
            contactIds.add(orderItem.Student__c);
        }

        System.assertEquals(300, [SELECT Rewards_Points__c FROM Contact WHERE Id IN :contactIds].Rewards_Points__c);

        orderLineItemsToUpdate.get(2).Shipping_Status__c = 'Cancelled';

        Test.startTest();
        update ( orderLineItemsToUpdate );
        Test.stopTest();

        System.assertEquals(200, [SELECT Rewards_Points__c FROM Contact WHERE Id IN :contactIds].Rewards_Points__c);
    }

    @isTest static void insert_multipleOrderLineItems_checkProduct2Parameters(){

        Account acct = [SELECT Name FROM Account WHERE Name = 'testAccountName'];

        Book_Order__c bookOrderFromSetup = [SELECT Name FROM Book_Order__c WHERE Name = 'testBookOrderName' AND Account__c = :acct.Id];
        insert ( new Product2__c(Name = 'testProduct2Name') );
        Product2__c product2 = [SELECT Units_Sold__c, Units_On_Order__c FROM Product2__c WHERE Name = 'testProduct2Name'];

        List<Order_Line_Item__c> orders = new List<Order_Line_Item__c>();

        for(Integer i = 0; i < 100; i++){
            orders.add(new Order_Line_Item__c(Name = 'testOrderItem', Shipping_Status__c = 'Shipped', Student__c =  '0030Y00000rPeEt',
                                              Level__c = 'Gold', Quantity__c = 10, Book_Order__c = bookOrderFromSetup.Id,
                                              Product__c = product2.Id));

            orders.add(new Order_Line_Item__c(Name = 'testOrderItem', Shipping_Status__c = 'Pending', Student__c =  '0030Y00000rPeEt',
                                              Level__c = 'Gold', Quantity__c = 10, Book_Order__c = bookOrderFromSetup.Id,
                                                Product__c = product2.Id));
        }

        Test.startTest();
        insert ( orders );
        Test.stopTest();

        System.assertEquals(100, [SELECT Units_Sold__c FROM Product2__c WHERE Id = :product2.Id].Units_Sold__c);
        System.assertEquals(100, [SELECT Units_On_Order__c FROM Product2__c WHERE Id = :product2.Id].Units_On_Order__c);
    }

    @isTest static void update_orderLineItem_checkProduct2Parameters_changeProductLookup(){

        List<Order_Line_Item__c> orderLineItemsToUpdate = new List<Order_Line_Item__c>();

        insert (new Product2__c(Name = 'name2820'));
        Product2__c product2 = [SELECT Units_Sold__c, Units_On_Order__c FROM Product2__c WHERE Name = 'name2820'];

        orderLineItemsToUpdate = [SELECT Quantity__c, Rewards_Points__c, Student__c FROM Order_Line_Item__c WHERE Quantity__c = 20];

        System.assertEquals(3, [SELECT Units_Sold__c FROM Product2__c WHERE Name = 'testProduct2setup'].Units_Sold__c);
        System.assertEquals(2, [SELECT Units_On_Order__c FROM Product2__c WHERE Name = 'testProduct2setup'].Units_On_Order__c);
        System.debug('product2.Id = ' + product2.Id);
        orderLineItemsToUpdate.get(2).Product__c = product2.Id;

        Test.startTest();
        update ( orderLineItemsToUpdate );
        Test.stopTest();

        System.assertEquals(2, [SELECT Units_Sold__c FROM Product2__c WHERE Name = 'testProduct2setup'].Units_Sold__c);
        System.assertEquals(2, [SELECT Units_On_Order__c FROM Product2__c WHERE Name = 'testProduct2setup'].Units_On_Order__c);

        System.assertEquals(1, [SELECT Units_Sold__c FROM Product2__c WHERE Name = 'name2820'].Units_Sold__c);
        System.assertEquals(null, [SELECT Units_On_Order__c FROM Product2__c WHERE Name = 'name2820'].Units_On_Order__c);
    }

    @isTest static void update_orderLineItem_checkProduct2Parameters_Cancelled(){

        List<Order_Line_Item__c> orderLineItemsToUpdate = new List<Order_Line_Item__c>();

        orderLineItemsToUpdate = [SELECT Shipping_Status__c FROM Order_Line_Item__c WHERE Quantity__c = 20];

        orderLineItemsToUpdate.get(1).Shipping_Status__c = 'Cancelled';

        Test.startTest();
        update ( orderLineItemsToUpdate );
        Test.stopTest();

        System.assertEquals(3, [SELECT Units_Sold__c FROM Product2__c WHERE Name = 'testProduct2setup'].Units_Sold__c);
        System.assertEquals(1, [SELECT Units_On_Order__c FROM Product2__c WHERE Name = 'testProduct2setup'].Units_On_Order__c);
    }

    @isTest static void update_orderLineItem_checkProduct2Parameters_ShippingStatus_null(){

        List<Order_Line_Item__c> orderLineItemsToUpdate = new List<Order_Line_Item__c>();

        orderLineItemsToUpdate = [SELECT Shipping_Status__c FROM Order_Line_Item__c WHERE Quantity__c = 20];

        orderLineItemsToUpdate.get(1).Shipping_Status__c = null;

        Test.startTest();
        update ( orderLineItemsToUpdate );
        Test.stopTest();

        System.assertEquals(3, [SELECT Units_Sold__c FROM Product2__c WHERE Name = 'testProduct2setup'].Units_Sold__c);
        System.assertEquals(1, [SELECT Units_On_Order__c FROM Product2__c WHERE Name = 'testProduct2setup'].Units_On_Order__c);
    }

    @isTest static void update_orderLineItem_checkProduct2Parameters_ShippingStatus_Shipped(){

        List<Order_Line_Item__c> orderLineItemsToUpdate = new List<Order_Line_Item__c>();

        orderLineItemsToUpdate = [SELECT Shipping_Status__c FROM Order_Line_Item__c WHERE Quantity__c = 20];

        orderLineItemsToUpdate.get(2).Shipping_Status__c = 'Shipped';

        Test.startTest();
        update ( orderLineItemsToUpdate );
        Test.stopTest();

        System.assertEquals(3, [SELECT Units_Sold__c FROM Product2__c WHERE Name = 'testProduct2setup'].Units_Sold__c);
        System.assertEquals(2, [SELECT Units_On_Order__c FROM Product2__c WHERE Name = 'testProduct2setup'].Units_On_Order__c);
    }

    @isTest static void check_getTotalPages(){

        List<Order_Line_Item__c> orders = new List<Order_Line_Item__c>();

        Account acct = [SELECT Name FROM Account WHERE Name = 'testAccountName'];

        Book_Order__c bookOrderFromSetup = [SELECT Name FROM Book_Order__c WHERE Name = 'testBookOrderName' AND Account__c = :acct.Id];
        for(Integer i = 0; i < 100; i++){
            orders.add(new Order_Line_Item__c(Name = 'testOrderItem', Shipping_Status__c = 'Shipped', Student__c =  '0030Y00000rPeEt',
                                              Level__c = 'Gold', Quantity__c = 10, Book_Order__c = bookOrderFromSetup.Id));
        }

        Test.startTest();
        insert ( orders );
        Test.stopTest();

        ApexPages.currentPage().getParameters().put('orderId', bookOrderFromSetup.Id);
        OrderDetailPageController2 customPageController = new OrderDetailPageController2();

        System.assertEquals(11, customPageController.getNumberOfPages());
    }

    @isTest static void check_cancel(){

        Account acct = [SELECT Name FROM Account WHERE Name = 'testAccountName'];

        Book_Order__c bookOrderFromSetup = [SELECT Name FROM Book_Order__c WHERE Name = 'testBookOrderName' AND Account__c = :acct.Id];
        Test.startTest();
        ApexPages.currentPage().getParameters().put('orderId', bookOrderFromSetup.id);
        OrderDetailPageController2 customPageController = new OrderDetailPageController2();
        Test.stopTest();

        System.assertEquals(String.valueOf(new PageReference(new PageReference('/' + bookOrderFromSetup.Id).getUrl())).trim(),
                            String.valueOf(customPageController.cancel()).trim());
    }

    @isTest static void check_pagination(){
        OrderDetailPageController2 customPageController = new OrderDetailPageController2();

        customPageController.getOrderLineItems();
        customPageController.next();
        customPageController.first();
        customPageController.last();
        customPageController.previous();
        customPageController.cancel();
    }

    @isTest static void check_getBookOrder(){
        Account acct = [SELECT Name FROM Account WHERE Name = 'testAccountName'];

        Book_Order__c bookOrderFromSetup = [SELECT Name, Account__r.Id FROM Book_Order__c WHERE Name = 'testBookOrderName' AND Account__c = :acct.Id];
        ApexPages.currentPage().getParameters().put('orderId', bookOrderFromSetup.id);
        OrderDetailPageController2 OrderDetailPageController = new OrderDetailPageController2();

        System.assertEquals(bookOrderFromSetup.Account__r.Id, orderDetailPageController.getBookOrder().Account__r.Id);
    }

    @isTest static void check_sortRecord(){
        List<Order_Line_Item__c> orderLineItems = new List<Order_Line_Item__c>();
        List<Order_Line_Item__c> sortedOrderLineItems = new List<Order_Line_Item__c>();

        Account acct = [SELECT Name FROM Account WHERE Name = 'testAccountName'];

        Book_Order__c bookOrderFromSetup = [SELECT Name FROM Book_Order__c WHERE Name = 'testBookOrderName' AND Account__c = :acct.Id];
        ApexPages.currentPage().getParameters().put('orderId', bookOrderFromSetup.id);

        String selectedField = 'cont_1';

        List<Contact> contactFromSetup = [SELECT LastName, FirstName FROM Contact WHERE FirstName = 'fromSetup'];

        for(Integer i = 0; i < contactFromSetup.size() - 1; i++){
            orderLineItems.add(new Order_Line_Item__c(Name = 'testOrderItem', Shipping_Status__c = 'Shipped', Student__c =  contactFromSetup.get(i).Id,
                                              Level__c = 'Gold', Quantity__c = 10, Book_Order__c = bookOrderFromSetup.Id));
        }

        Test.startTest();
        insert ( orderLineItems );
        Test.stopTest();

        sortedOrderLineItems = [SELECT Id FROM Order_Line_Item__c WHERE Student__r.Name = :selectedField];
    }

    @isTest static void check_save(){

        OrderDetailPageController2 orderDetailPageController = new OrderDetailPageController2();

        Account acct = [SELECT Name FROM Account WHERE Name = 'testAccountName'];

        Book_Order__c bookOrderFromSetup = [SELECT Name FROM Book_Order__c WHERE Name = 'testBookOrderName' AND Account__c = :acct.Id];
        ApexPages.currentPage().getParameters().put('orderId', bookOrderFromSetup.id);

        System.assertEquals(3, [SELECT Shipping_Status__c FROM Order_Line_Item__c WHERE Shipping_Status__c = 'Shipped'].size());

        for(OrderLineItem orderItem :orderDetailPageController.getOrderLineItems()){
            if(orderItem.checked == true) continue;
               orderItem.checked = true;
        }

        orderDetailPageController.save();

        orderDetailPageController.getOrderLineItems();

        for(Integer i = 0; i < orderDetailPageController.getOrderLineItems().size(); i++){
            System.assertEquals('Shipped', orderDetailPageController.getOrderLineItems().get(i).orderLineItem.Shipping_Status__c);
        }

    }

}