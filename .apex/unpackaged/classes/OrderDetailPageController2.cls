public with sharing class OrderDetailPageController2 {

    public List<OrderLineItem> orders {get; set;}
    public List<SelectOption> options {get;set;}
    public Set<Id> selectedOrderLineItemIds {get;set;}

    public Integer pageNum {get; set;}
    public Integer limitSize {get; set;}

    Integer totalSize = 0;

    public String contextItem {get; set;}
    public Boolean checkboxStatus {get; set;}
    public String selectedField {get; set;}

    private String statusCancelled = OrderDetailConstants.STATUS_CANCELLED;

    private Id orderID = ApexPages.currentPage().getParameters().get('orderId');

    public OrderDetailPageController2(){
        limitSize = 10;
        selectedOrderLineItemIds = new Set<Id>();
        getOrderLineItems();
        getContactUniqueNames(getAllOrderLineItems());
    }

    public ApexPages.StandardSetController con {
        get {
        String queryString = (selectedField == null) ? OrderDetailConstants.QUERY + OrderDetailConstants.ORDER_QUERY
                :OrderDetailConstants.QUERY + OrderDetailConstants.QUERY_SELECTED_FIELD + OrderDetailConstants.ORDER_QUERY;

            if(con == null) {
                con = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
                pageNum = 1;
                con.setPageSize(limitSize);
                totalSize = con.getResultSize();
            }
            return con;
        }
        set;
    }

    Book_Order__c b;

    public Book_Order__c getBookOrder(){
        if (b == null) b = [SELECT Name, Account__c, Account__r.Name, Account__r.billingStreet, Account__r.billingState,
                Account__r.billingPostalcode, Account__r.billingCountry
                FROM Book_Order__c WHERE Id = :orderID];
        return b;
    }

    public void verifyCheckboxStatus(){
        if(checkboxStatus == true){
            doSelectItem();
        } else {
            doDeselectItem();
        }
    }

    public void doSelectItem(){this.selectedOrderLineItemIds.add(this.contextItem);}

    public void doDeselectItem(){this.selectedOrderLineItemIds.remove(this.contextItem);}

    public List<OrderLineItem> getOrderLineItems(){

        if(selectedField != null)
            sortRecords();
        else
            orders = getAllOrderLineItems();
        return orders;
    }

    public void sortRecords(){
        orders.clear();
        con = null;
        getRecords();
    }

    public List<OrderLineItem> getAllOrderLineItems(){
        orders = new List<OrderLineItem>();
        getRecords();
        return orders;
    }

    public PageReference save(){

        List<Order_Line_Item__c> orderLineItemsToUpdate = new List<Order_Line_Item__c>();
        Map<Id, Order_Line_Item__c> Id2OrderLineItem = new Map<Id, Order_Line_Item__c>();

        for(OrderLineItem cOrder :orders){
            Order_Line_Item__c newOrderLineItem;
            if(cOrder.checked == true && cOrder.orderLineItem.Shipping_Status__c != OrderDetailConstants.STATUS_SHIPPED) {
                newOrderLineItem = new Order_Line_Item__c(
                        id = cOrder.orderLineItem.Id,
                        Shipping_Status__c = OrderDetailConstants.STATUS_SHIPPED,
                        Date_Shipped__c = Date.today()
                );
            }
            Id2OrderLineItem.put(cOrder.orderLineItem.Id, newOrderLineItem);
        }
        for(Order_Line_Item__c orderItem :[SELECT Shipping_Status__c FROM Order_Line_Item__c WHERE Id IN :Id2OrderLineItem.keySet()]){
            if(Id2OrderLineItem.containsKey(orderItem.Id) && Id2OrderLineItem.get(orderItem.Id) != null) {
                orderLineItemsToUpdate.add(Id2OrderLineItem.get(orderItem.Id));
            }
        }
        update ( orderLineItemsToUpdate );
        return new PageReference(new PageReference('/' + orderID).getUrl());
    }

    public PageReference cancel(){
        con.cancel();
        return new PageReference(new PageReference('/' + orderID).getUrl());
    }

    public Boolean hasNext {
        get { return con.getHasNext(); }
        set;
    }

    public Boolean hasPrevious {
        get { return con.getHasPrevious(); }
        set;
    }

    public void first() {
        pageNum = 1;
        con.first();
    }

    public void last() {
        if(pageNum != getNumberOfPages())
            pageNum = getNumberOfPages();
        con.last();
    }

    public void previous() {
        if(pageNum != 1)
            pageNum--;
        con.previous();
    }

    public void next() {
        if(pageNum != getNumberOfPages()) pageNum++;
        con.next();
    }

    public Integer getNumberOfPages(){
        //?????
        return (Math.mod(totalSize, limitSize) == 0) ? totalSize / limitSize : Math.abs(totalSize / limitSize) + 1;
    }

    private void assignCheckStatus(Order_Line_Item__c orderItem){
        OrderLineItem currentWrap = new OrderLineItem(orderItem);
        if(orderItem.Shipping_Status__c != OrderDetailConstants.STATUS_SHIPPED && this.selectedOrderLineItemIds != null){
            if(this.selectedOrderLineItemIds.contains(orderItem.Id))
                currentWrap.checked = true;
            else
                currentWrap.checked = false;
        }
        orders.add(currentWrap);
    }

    private List<SelectOption> getContactUniqueNames(List<OrderLineItem> orderItems) {
        Set<String> name2ContactName = new Set<String>();
        options = new List<SelectOption>{new SelectOption('', '--All Students--')};
        for(Order_Line_Item__c order :[SELECT Student__r.Name FROM Order_Line_Item__c
                                        WHERE Book_Order__c = :orderID AND Shipping_Status__c != :statusCancelled
                                        ORDER BY Student__r.Name]) {
			name2ContactName.add(order.Student__r.Name); }
        for(String r : name2ContactName){
            options.add(new SelectOption(r, r));
        }
        return options;
    }

    private void getRecords(){for(Order_Line_Item__c orderItem : (List<Order_Line_Item__c>)con.getRecords()){assignCheckStatus(orderItem);}}

}