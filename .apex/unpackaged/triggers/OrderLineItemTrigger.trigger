trigger OrderLineItemTrigger on Order_Line_Item__c (before insert, before update, before delete, after insert, after update, after delete) {

	if (Trigger.isBefore) {
		if (Trigger.isInsert || Trigger.isUpdate) {
			OrderLineItemTriggerHelper.calculateRewardsForOrderLineItem(OrderLineItemTriggerHelper.filterOrderLineItems(Trigger.oldMap, Trigger.new));
		}
	}

	if (Trigger.isAfter) {
		if (Trigger.isInsert || Trigger.isUpdate || Trigger.isDelete) {

			OrderLineItemTriggerHelper.getOldContactLookups(Trigger.oldMap);
			OrderLineItemTriggerHelper.getOldProduct2Lookups(Trigger.oldMap);

			OrderLineItemTriggerHelper.calculateRewardsForContact(OrderLineItemTriggerHelper.filterOrderLineItems(Trigger.oldMap, Trigger.new));
			OrderLineItemTriggerHelper.calculateParametersForProduct2(OrderLineItemTriggerHelper.filterOrderLineItems(Trigger.oldMap, Trigger.new));

		}
	}
}