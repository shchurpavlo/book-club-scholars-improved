public class OrderDetailConstants {
    
    public static final String QUERY = 'SELECT Quantity__c, Shipping_Status__c, Book_Order__r.Name, Book_Order__r.Account__r.Name, Book_Order__r.Account__r.BillingStreet, ' + 
        ' Book_Order__r.Account__r.BillingState, Book_Order__r.Account__r.BillingPostalcode, Book_Order__r.Account__r.BillingCountry, ' + 
        ' Date_Shipped__c, Product__c, Product__r.Name, Student__c, Student__r.Name ' + 
        ' FROM Order_Line_Item__c WHERE Book_Order__c = :orderID AND Shipping_Status__c != :statusCancelled';
    
    
    public static final String ORDER_QUERY = ' ORDER BY Student__r.Name, Product__r.Name';
    public static final String QUERY_SELECTED_FIELD = ' AND Student__r.Name = :selectedField';
    
    public static final String STATUS_SHIPPED = 'Shipped';
    public static final String STATUS_PENDING = 'Pending';
    public static final String STATUS_CANCELLED = 'Cancelled';
    
}