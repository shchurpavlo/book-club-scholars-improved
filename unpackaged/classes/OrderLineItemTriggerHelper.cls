public class OrderLineItemTriggerHelper {

	public static Set<Id> oldContactIds = new Set<Id>();
	public static Set<Id> oldProduct2Ids = new Set<Id>();

	public static Map<String, Rank__c> rank{get {return Rank__c.getAll();}}

	public static List<Order_Line_Item__c> filterOrderLineItems(Map<Id, Order_Line_Item__c> oldMap,
																List<Order_Line_Item__c> newList) {
		List<Order_Line_Item__c> itemsToUpdate = new List<Order_Line_Item__c>();

		for (Order_Line_Item__c item :(newList == null) ? oldMap.values() : newList) {
			if (oldMap == null || newList == null || (oldMap.get(item.Id).Level__c != item.Level__c ||
							oldMap.get(item.Id).Quantity__c != item.Quantity__c ||
							oldMap.get(item.Id).Shipping_Status__c != item.Shipping_Status__c ||
							oldMap.get(item.Id).Student__c != item.Student__c ||
							oldMap.get(item.Id).Product__c != item.Product__c)) {
				itemsToUpdate.add(item);
			}
		}
		return itemsToUpdate;
	}

	public static void calculateRewardsForOrderLineItem(List<Order_Line_Item__c> orderLineItems) {
		for (Order_Line_Item__c item :orderLineItems) {item.Rewards_Points__c = calculateReward(item);}
	}

	public static void calculateRewardsForContact(List<Order_Line_Item__c> orderLineItems) {
		if (orderLineItems.isEmpty()) return;

		List<Contact> contactsToUpdate = new List<Contact>();
		Set<Id> studentIds = new Set<Id>();

		for (Order_Line_Item__c item :orderLineItems) {
			if (item.Student__c == null) continue;
			studentIds.add(item.Student__c);
		}

		studentIds.addAll(oldContactIds);
		Map<Id, Decimal> studentId2Rewards = rewardForContact(studentIds);

		for (Id contactId :studentIds) {
			contactsToUpdate.add(
				new Contact(
					Id = contactId,
					Rewards_Points__c = studentId2Rewards.get(contactId)
				));
		}
		update ( contactsToUpdate );
	}

	public static void calculateParametersForProduct2(List<Order_Line_Item__c> orderLineItems) {
		if (orderLineItems.isEmpty()) return;

		List<Product2__c> productItems = new List<Product2__c>();

		Set<Id> productIds = new Set<Id>();

		for (Order_Line_Item__c item :orderLineItems) {
			if (item.Product__c == null) continue;
			productIds.add(item.Product__c);
		}
		productIds.addAll(oldProduct2Ids);

		Map<Id, Map<String, Integer>> resultMap = getParametersForProduct(productIds);

		for (Id productId :productIds) {
			productItems.add(new Product2__c(
					Id = productId,
					Units_Sold__c = (resultMap.get(productId) == null) ? null : resultMap.get(productId).get(OrderDetailConstants.STATUS_SHIPPED),
					Units_On_Order__c = (resultMap.get(productId) == null) ? null : resultMap.get(productId).get(OrderDetailConstants.STATUS_PENDING)
			));
		}
		update ( productItems);
	}

	private static Integer calculateReward(Order_Line_Item__c orderLineItem) {
		Integer result;
		if (orderLineItem.Shipping_Status__c != OrderDetailConstants.STATUS_SHIPPED) return result;
		if (orderLineItem.Level__c == null || orderLineItem.Quantity__c == null || rank.get(orderLineItem.Level__c) == null) return result;
		result = Integer.valueOf(rank.get(orderLineItem.Level__c).Reward__c) *
				Integer.valueOf(orderLineItem.Quantity__c);
		return result;
	}

	private static Map<Id, Decimal> rewardForContact(Set<Id> studentIds) {
		Map<Id, Decimal> studentId2Rewards = new Map<Id, Decimal>();
		for (AggregateResult a :[SELECT SUM(Rewards_Points__c) r, Student__c studentId
									FROM Order_Line_Item__c WHERE Student__c IN :studentIds
									AND Shipping_Status__c = :OrderDetailConstants.STATUS_SHIPPED GROUP BY Student__c]) {
			studentId2Rewards.put((Id) a.get('studentId'), (Decimal) a.get('r'));
		}
		return studentId2Rewards;
	}

	private static Map<Id, Map<String, Integer>> getParametersForProduct(Set<Id> productIds) {
		Map<Id, Map<String, Integer>> resultMap = new Map<Id, Map<String, Integer>>();

		for (AggregateResult a :[SELECT COUNT(Id) orders, Product__c productId, Shipping_Status__c status FROM Order_Line_Item__c
								 WHERE Product__c IN :productIds GROUP BY Product__c, Shipping_Status__c]) {
			Map<String, Integer> orderStatusToNumberOfOrders = new Map<String, Integer>();
			orderStatusToNumberOfOrders.put((String) a.get('status'), (Integer) a.get('orders'));
			if(resultMap.get((Id) a.get('productId')) == null){
				resultMap.put((Id) a.get('productId'), orderStatusToNumberOfOrders);
			} else {
				resultMap.get((Id) a.get('productId')).putAll(new Map<String, Integer>());
			}
			resultMap.get((Id) a.get('productId')).putAll(orderStatusToNumberOfOrders);
		}
		return resultMap;
	}

	public static void getOldContactLookups(Map<Id, Order_Line_Item__c> oldMap) {
		if (oldMap == null) return;

		for (Order_Line_Item__c orderItem :oldMap.values()) {
			if (orderItem.Student__c == null) continue;
			oldContactIds.add(orderItem.Student__c);
		}
	}

	public static void getOldProduct2Lookups(Map<Id, Order_Line_Item__c> oldMap) {
		if (oldMap == null) return;

		for (Order_Line_Item__c orderItem :oldMap.values()) {
			if (orderItem.Product__c == null) continue;
			oldProduct2Ids.add(orderItem.Product__c);
		}
	}

}