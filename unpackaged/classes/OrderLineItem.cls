public class OrderLineItem {
    
    public Order_Line_Item__c orderLineItem {get; set;}
    public Boolean checked {get; set;} 
    
    public OrderLineItem(Order_Line_Item__c order){
        orderLineItem = order;
        checked = (order.Shipping_Status__c == OrderDetailConstants.STATUS_SHIPPED) ? true : false;
    }
    
}